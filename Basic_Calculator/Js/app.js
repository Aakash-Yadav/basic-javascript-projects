let result_ = document.getElementById("input_text");

function calc(number) {
    result_.value += number;
}

function result() {
    try {
        result_.value = eval(result_.value);
    } catch (err) {
        alert("Enter the valid result");
        result_.value = "";
    }
}

function cls() {
    result_.value = "";
}
