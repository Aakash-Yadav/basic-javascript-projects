
## Basic JavaScript Calculator Project
The objective of this project is to code a basic arithmetic JavaScript calculator. This calculator adds, subtracts, multiplies, and divides.
#### JavaScript Used

- inline JavaScript
- JavaScript eval() function
- JavaScript assignment operator

## Result;
![alt text](https://xp.io/storage/5UcY2yB.png)
