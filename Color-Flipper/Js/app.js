const hex = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F"];


function hexColor() {
    let hexColor_ = "#";
    for (let i = 0; i < 6; i++) {
        hexColor_ += hex[random_numbers()];
    }
    return hexColor_;
}

function random_numbers() {
    let x = Math.floor(Math.random() * hex.length);
    return x;
}

function change_color() {
    let hex = hexColor()
    document.getElementById('name').innerHTML = `Color : ${(hex)}`;
    document.body.style.backgroundColor = hex;
}