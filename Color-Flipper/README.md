 
## create a Color Flipper
 ![alt text](https://xp.io/storage/2722zTYL.png)

 By manipulating the DOM, you have infinite possibilities. You can create applications that update the data of the page without needing a refresh. Also, you can create applications that are customizable by the user and then change the layout of the page without a refresh.
 
 ### Key concepts covered:
 - **arrays**
- **document.getElementById()**
- **document.querySelector()**
- **addEventListener()**
- **document.body.style.backgroundColor**
- **Math.floor()**
- **Math.random()**
- **array.length**

### OutPut -:
![alt text](https://xp.io/storage/272gckHP.png)
 
## thank you