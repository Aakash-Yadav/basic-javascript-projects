# Bubble sort
![alt text](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27835/bubble-sort-295a6abfc3ca865791b985101ed9358f.gif)

##### Bubble sort, sometimes referred to as sinking sort, is a simple sorting algorithm that repeatedly steps through the list, compares adjacent elements and swaps them if they are in the wrong order. The pass through the list is repeated until the list is sorted.

# Implementing Bubble Sort in Javascript
It is the sorting algorithm that performs by repeated sorts of an array element by comparing the adjacent elements. It compares the adjacent element, and it swaps the element if they are in the wrong order. This algorithm repeatedly runs until all the elements in the lists are sorted. If all the elements sorted in the list, then the algorithms end automatically. Although the bubble sort algorithm is simple, it consumes more time comparing other sorting techniques. IKn this topic, we are going to learn about Bubble Sort in JavaScript.

## How does Bubble Sort work in JavaScript?
- In Bubble Sort, the algorithm will take the 1st element of the array and compare the value with the element next to it in the array.
- If the 1st element is larger than the 2nd, then the element will swap the positions.
- If it doesn’t satisfy the condition, then the algorithm will compare the 2nd element with 3rd
- This process continues until all the elements in the array is bubble sorted in the respective order.

