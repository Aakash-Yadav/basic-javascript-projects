function Bubble_Sort(list) {
    if (list.length <= 1) {
        return list;
    } else {
        while (true) {
            let find = false;
            for (let i = 0; i < list.length - 1; i++) {
                if (list[i] > list[i + 1]) {
                    let x = list[i];
                    list[i] = list[i + 1];
                    list[i + 1] = x;
                    find = true;
                } else {
                    continue;
                }
            }
            if (!find) {
                return list;
            }
        }
    }
}

const list = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, -1, -9, -6, -4, -2, -3, -5, 0];
console.log("Unsorted Array =", list);
console.log("Bubble sort =", Bubble_Sort(list));
