function splitter(left, right) {
    const result = [];
    let Li = 0;
    let Rj = 0;
    while (left.length > Li && right.length > Rj) {
        if (left[Li] <= right[Rj]) {
            result.push(left[Li]);
            Li++;
        } else {
            result.push(right[Rj]);
            Rj++;
        }
    }
    while (left.length > Li) {
        result.push(left[Li]);
        Li++;
    }
    while (right.length > Rj) {
        result.push(right[Rj]);
        Rj++;
    }
    return result;
}

function merge_sort(n) {
    if (n.length <= 1) {
        return n;
    } else {
        let mid = Math.floor(n.length / 2);
        const left = merge_sort(n.slice(0, mid));
        const right = merge_sort(n.slice(mid, n.length));
        return splitter(left, right);
    }
}
const data = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, -1, -9, -6, -4, -2, -3, -5, 0];
console.log("Unsorted Array =", data);
console.log("Merge Sort =", merge_sort(data));