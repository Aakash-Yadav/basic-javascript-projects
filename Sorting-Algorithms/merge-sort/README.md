# Merge sort ;
In computer science, merge sort is an efficient, general-purpose, and comparison-based sorting algorithm. Most implementations produce a stable sort, which means that the order of equal elements is the same in the input and output. Merge sort is a divide and conquer algorithm that was invented by John von Neumann in 1945. A detailed description and analysis of bottom-up merge sort appeared in a report by Goldstine and von Neumann as early as 1948.

### Merge sort in javascript ;
Learn what is merge sort and how to implement merge sort algorithm in javascript.

Sorting is one of the most prominent algorithms and building blocks of computer science, we use it more often than any algorithms out there, There is always a race to create a fast sorting algorithm.

## How merge sort works?
##### Conceptually merge sorts works as follows:
- Divide the given unsorted elements into n sub element list, each containing only one element (A single element is always sorted).
- Repeatedly merge this sub element lists in the required order until there is only one sub element list. At the end the last list will be the sorted.

![alt text](https://rb.gy/ndddpq)

## Top down merge sort
As in each iteration it first breaks the list into sublist and then sort and merges them, which basically means it is moving from top to down.

Each problem is broken into part, which further breaks into parts until it is easy to solve.
## Merging the sorted list
As merge sort uses divide and conquer paradigm to sort the list, merging can be stated as the conquer part.

To merge the sorted sub element list, we compare all the elements of the both the list and then add the elements into temp array in the order.

After comparing and merging both the lists, if there elements left in either of the sub list then we have to add that as well in the temp array and then return this merged list.

### Time Complexity of merge sort
If you closely monitor the working of merge sort then in each call it breaks the list in the half and then merges it.

![alt text](https://xp.io/storage/2FOEJr6d.png)