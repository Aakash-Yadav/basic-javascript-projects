function find_the_low_value(list) {
  let low = list[0];
  for (let data of list) {
    if (data < low) {
      low = data;
    } else {
      continue;
    }
  }
  return low;
}

function selection_sort(list) {
  if (list.length < 1) {
    return list;
  } else {
    for (let i = 0; i < list.length - 1; i++) {
      let low_value = list.indexOf(
        find_the_low_value(list.slice(i, list.length))
      );
      let x = list[i];
      list[i] = list[low_value];
      list[low_value] = x;
    }
    return list;
  }
}
const list = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, -1, -9, -6, -4, -2, -3, -5, 0];

console.log("Unsorted Array =", list);
console.log("Selection sort =", selection_sort(list));
