# Selection sort
In computer science, selection sort is an in-place comparison sorting algorithm. It has an O time complexity, which makes it inefficient on large lists, and generally performs worse than the similar insertion sort
## Flowchart of the Selection Sort: 
![alt text ](https://media.geeksforgeeks.org/wp-content/cdn-uploads/20220203094305/Selection-Sort-Flowhchart.png)

![alt text](https://xp.io/storage/2EpDwHev.png)
## Introduction to Selection sort in JavaScript

Selection sort in JavaScript is one of the most simplest and intuitive sorting algorithms. It works to repeatedly find minimum elements in the unsorted, considering ascending order, and move to the start of the array. In the Computer field, there are some tools that are often used and based on Algorithms internally. As programmers, we move through data that is built with modern programming languages available in one way or another. Using these built-in sorting algorithms, work becomes easier for a programmer but it is still necessary to understand what is going on underneath and what are all the different types of sorting algorithms. So today, we shall deep dive into the Selection sort algorithm, which is one fundamental way of sorting algorithms.

## How does Selection Sort work?
We shall see How Selection Sort works with few examples,
It is a comparison-based algorithm, which divides into two parts. One is the sorted part and the other being the unsorted part. Initially, sorted part is empty. The first element from the unsorted part is compared and swapped with the before element which actually adds to the sorted list, and the process goes on.

## animation
![alt text](https://cdn.emre.me/sorting/selection_sort.gif)