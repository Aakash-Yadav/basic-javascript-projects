# Insertion sort algorithm ;
![alt text](https://rb.gy/er6zpt)
Insertion sort is a simple sorting algorithm that builds the final sorted array one item at a time. It is much less efficient on large lists than more advanced algorithms such as quicksort, heapsort, or merge sort

#### simple implementation of insertion sort algorithm .
Insertion sort is one of the simplest and most widely used sorting algorithm.

Just imaging a deck of card in your hands and how will you sort these cards?.

The simple answer is we will pick the smallest card and place it at first position, then second smallest at second position, third smallest at third and so on depending upon the sorting order.

It also sorts the elements by placing the smallest element at first place then second smallest at second place and so on.

![alt text](https://xp.io/storage/2FNxMMUy.png)

## Insertion Sort Implementation in javascript

- We will use two nested loops to sort the elements.
- In the first loop, we will start the loop from the second element as we need to check it with the first element for sorting.
- In the second loop we will iterate all the elements before the current element of the first loop and swap them if it is less than or greater than based on the sorting order.

