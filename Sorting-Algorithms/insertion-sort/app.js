function insertion_sort(data) {
    if (data.length <= 1) {
        return data;
    } else {
        for (let i = 1; i < data.length; i++) {
            let j = i;
            while (data[j - 1] > data[j] && j > 0) {
                let x = data[j];
                data[j] = data[j - 1];
                data[j - 1] = x;
                j--;
            }
        }
        return data;
    }
}

const data = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, -1, -9, -6, -4, -2, -3, -5, 0];
console.log("Unsorted Array =", data);
console.log("Insertion Sort =", insertion_sort(data))