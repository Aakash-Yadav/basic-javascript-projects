function increase() {
  let number = +document.getElementById("number").innerHTML;
  number++;
  let data = document.getElementById("number");
  data.innerHTML = number;
  data.style = "color:green";
}

function reset() {
  let x = document.getElementById("number");
  x.innerHTML = 0;
  x.style = "color:plum";
}

function decrease() {
  let number_2 = +document.getElementById("number").innerHTML;
  if (number_2 == 0) {
    reset();
  } else {
    number_2--;
    let data = document.getElementById("number");
    data.innerHTML = number_2;
    data.style = "color:red";
  }
}
