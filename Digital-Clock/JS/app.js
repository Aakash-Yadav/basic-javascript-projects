

function add_zero(n){
    if(n<10){
        n = '0'+n;
        return n ;
    }else{
        return n ;
    }
}

function mytime() {
    //hours minutes seconds session
    let current_date = new Date();
    let hour = current_date.getHours();
    let minute = current_date.getMinutes();
    let second = current_date.getSeconds();
    let session = (hour >= 12) ? "PM" : "AM";

    let result = `${add_zero(hour)} : ${add_zero(minute)} : ${add_zero(second)}  ${session}`;

    document.getElementById('showtime').innerHTML = result;
    
    setTimeout(mytime,1000)
}
mytime();