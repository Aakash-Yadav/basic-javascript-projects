## Digital Clock in 24-Hour format
In the 24-hour format, time is displayed in the form of HH : MM : SS. In the 12-hour format, it is displayed in the form of HH : MM : SS AM/PM.

## The Structure
To begin with, create a div with id clock in which you want to display time. We will insert the time into this div using JavaScript.

# The Styling
he styling for the text to be displayed in the div is defined in the CSS.

## Out-Put:
![alt text](https://xp.io/storage/2EjQqKBF.png)